
## [v1.0.2](https://bitbucket.org/roitinnovation/roit-queues/compare/v1.0.2..v1.0.1)

2023-04-18

### Chore

* **jenkinsfile:** remove useless functions
* **package:** update version in package
* **package:** update version in package
* **package:** update version in package
* **package:** update version in package

### Docs

* **changelog:** generated CHANGELOG.md file

### Feat

* **lib-internal:** converts to new libs internal structure

### Pull Requests

* Merged in conflict/bugfix/RBANK-14894 (pull request [#8](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/8/))
* Merged in bugfix/RBANK-14894 (pull request [#7](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/7/))
* Merged in bugfix/RBANK-14894 (pull request [#6](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/6/))
* Merged in bugfix/RBANK-14894 (pull request [#4](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/4/))
* Merged in feature/RBANK-5118 (pull request [#1](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/1/))


## [v1.0.1](https://bitbucket.org/roitinnovation/roit-queues/compare/v1.0.1..v1.0.3-preview)

2023-04-17

### Chore

* **package:** update version in package

### Docs

* **changelog:** generated CHANGELOG.md file
* **changelog:** generated CHANGELOG.md file
* **changelog:** generated CHANGELOG.md file

### Pull Requests

* Merged in bugfix/RBANK-14894 (pull request [#5](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/5/))


## [v1.0.3-preview](https://bitbucket.org/roitinnovation/roit-queues/compare/v1.0.3-preview..v1.0.2-preview)

2023-04-17

### Chore

* **package:** update version in package

### Feat

* **cloud-task-provider:** rename method

### Pull Requests

* Merged in bugfix/RBANK-14894 (pull request [#7](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/7/))


## [v1.0.2-preview](https://bitbucket.org/roitinnovation/roit-queues/compare/v1.0.2-preview..v1.0.1-preview)

2023-04-17

### Chore

* **package:** update version in package

### Feat

* **cloud-task-provider:** change to add null returnable

### Pull Requests

* Merged in bugfix/RBANK-14894 (pull request [#6](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/6/))


## [v1.0.1-preview](https://bitbucket.org/roitinnovation/roit-queues/compare/v1.0.1-preview..v1.0.0)

2023-04-17

### Chore

* **package:** update version in package

### Feat

* **cloud-task-provider:** add new method to getLatestTaskFromQueue
* **lib-internal:** converts to new libs internal structure

### Pull Requests

* Merged in bugfix/RBANK-14894 (pull request [#4](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/4/))
* Merged in feature/RBANK-5118 (pull request [#1](https://bitbucket.org/roitinnovation/roit-queues/pull-requests/1/))


## v1.0.0

2023-04-17

### Chore

* update deploy config
* update README.md
* add author and package name
* update package version
* release 0.0.16
* release 0.0.15
* add repository configuration
* update version
* add correct package version
* update readme
* add dependencies and package-lock.json on .gitignore
* **package:** update package version to 0.0.12
* **package:** update package version to 0.0.18
* **package:** update package version to 0.0.19
* **package.json:** release 0.0.17
* **update-lib-version:** increment lib version

### Feat

* delete task ajust
* add env.yaml example in the readme
* add initial config for cloud tasks
* add instructions to use cloudtask
* add cloudtask models
* add cloudtask logic to create task
* add params to scheduleTime
* ensure brokers always implements interfaces
* **auth:** add oidcToken service credential auth
* **auth:** version project
* **cloud-task:** export task type for task creation response
* **cloudtask:** add correct time parse to seconds on schedule
* **common-js-support:** adjust tsconfig.json to handle es6 modules
* **credentials:** add default initialization in instances of PubSub and CloudTasks
* **defaut-hedaer:** add default header content type json
* **pubsub:** create and expose pubsub handler
* **task-options:** http method PATCH ajust
* **task-options:** ajust create new task optional data
* **task-options:** ajust create new task optional data

### Fix

* change credential name and add instace of pubsubconfig
* change spelling error on README.md
* add missing body parse in constructor
* initialize classes
* **google-cloud-task:** remove service account from task object and get projectId directly from Environment
* **ms-import:** ajust import types ms
* **task:** add correct export type on ITask

### Refactor

* update readme
* remove unused files
* update readme
* change pubsub handler to receive object from interface implementation
* add interfaces folder

