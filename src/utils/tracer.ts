import { getTracerAgent } from '../otel'

export const startTracer = async <T>(
    name: string,
    operation: (span: any) => Promise<T>
): Promise<T> => {
  const tracerAgent = getTracerAgent()

  if (!tracerAgent) {
    return operation({ 
        setAttributes: () => {}, 
        end: () => {} 
    })
  }

  return await tracerAgent.getGlobalTracer().startActiveSpan(name, async (span: any) => {
    try {
        return await operation(span)
    } finally {
        span.end()
    }
  })
} 