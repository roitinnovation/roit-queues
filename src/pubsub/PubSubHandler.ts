import { PubSub } from '@google-cloud/pubsub'
import { PublisherInterface } from "../interfaces/PublisherInterface"
import { PubSubConfig } from './PubSubConfig'
import { startTracer } from '../utils/tracer'
import { getTracerAgent } from '../otel'

type Attributes = {
    [index: string]: string;
}

export class PubSubHandler implements PublisherInterface {

    private readonly instance: PubSub
    private readonly pubSubConfig: PubSubConfig = new PubSubConfig()

    constructor() {
        const config = this.pubSubConfig.getConfig()
        if (config) {
            this.instance = new PubSub(config)
        } else {
            this.instance = new PubSub()
        }
    }

    getInstance() {
        return this.instance
    }

    async publish(object: any, topicName: string, attributes?: Attributes): Promise<string> {
        return await startTracer('PubSub.Publish', async (span) => {
            const tracerAgent = getTracerAgent()
            if (tracerAgent) {
                const messageAttributes = attributes || {};
                tracerAgent.propagation.inject(tracerAgent.context.active(), messageAttributes);
                attributes = messageAttributes;
            }            
            try {
                const message = Buffer.from(JSON.stringify(object))
                const messageId = await this.getInstance()
                    .topic(topicName)
                    .publish(message, attributes)
                    .catch(err => {
                        console.error(err)
                        throw new Error('Error in PubSub publish method.')
                    })
                    .then(response => {
                        return response
                    })

                span.setAttributes({
                    'pubsub.topic': topicName,
                    'pubsub.success': true,
                    'pubsub.hasAttributes': !!attributes
                })

                return messageId
            } catch (error) {
                span.setAttributes({
                    'pubsub.topic': topicName,
                    'pubsub.success': false,
                    'pubsub.hasAttributes': !!attributes,
                    'pubsub.error': error instanceof Error ? error.message : 'Unknown error'
                })
                throw error
            }
        })
    }
}