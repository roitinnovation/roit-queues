import { BrokerConfigInterface } from '../interfaces/BrokerConfigInterface'

export class PubSubConfig implements BrokerConfigInterface {
    //@ts-ignore
    private readonly projectId: string
    //@ts-ignore
    private readonly credentials: { private_key: string, client_email: string }

    constructor() {
        const credentialFile = process.env.PUBSUBCREDENTIAL
        if (credentialFile) {
            this.projectId = process.env.PROJECTID
            this.credentials = require(credentialFile)
        }
    }

    getConfig(): any {
        return new PubSubConfig()
    }
}