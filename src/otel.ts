const getTracerAgent = () => {
  const agent = (global as any)._roit_trace_agent;
  if (!agent) {
    return null;
  }

  return agent;
}

export { getTracerAgent }
    