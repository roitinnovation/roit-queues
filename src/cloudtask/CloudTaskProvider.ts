import { CloudTasksClient } from "@google-cloud/tasks";
import { google } from "@google-cloud/tasks/build/protos/protos";
import { Task } from "./Task";
import { TaskConfiguration } from "./TaskConfiguration";
import moment from "moment"
const ms = require('ms')
import { newDate } from "@roit/roit-date"
import { parseISO } from 'date-fns'
import { startTracer } from '../utils/tracer'
import { getTracerAgent } from "../otel";

export class CloudTaskProvider {
    private instance: CloudTasksClient
    private projectId: string | undefined

    constructor() {
        this.projectId = process.env.PROJECTID
        this.instance = new CloudTasksClient({ fallback: true }) //https://github.com/googleapis/nodejs-tasks/issues/397#issuecomment-618580649
    }

    async getTasks(queue: string, region: string): Promise<google.cloud.tasks.v2.ITask[]> {
        if(!this.projectId) {
            return [];
        }

        return await startTracer('cloudtask.list', async (span) => {
            try {
                const parent = this.instance.queuePath(this.projectId!, region, queue)
                const [response] = await this.instance.listTasks({ parent })

                if (!response || response.length === 0) {
                    span.setAttributes({
                        'cloudtask.queue': queue,
                        'cloudtask.region': region,
                        'cloudtask.success': true,
                        'cloudtask.count': 0
                    })
                    return []
                }

                // This sort is needed because it is not returned in the correct order by default
                response.sort((a, b) => {
                    if (!a.createTime?.seconds || !b.createTime?.seconds) {
                        return 0
                    }
                    if (a.createTime.seconds > b.createTime.seconds) {
                        return -1
                    }
                    if (a.createTime.seconds < b.createTime.seconds) {
                        return 1
                    }
                    return 0
                })

                span.setAttributes({
                    'cloudtask.queue': queue,
                    'cloudtask.region': region,
                    'cloudtask.success': true,
                    'cloudtask.count': response.length
                })

                return response
            } catch (error) {
                span.setAttributes({
                    'cloudtask.queue': queue,
                    'cloudtask.region': region,
                    'cloudtask.success': false,
                    'cloudtask.error': error instanceof Error ? error.message : 'Unknown error'
                })
                throw error
            }
        })
    }

    async createTask(taskOptions: TaskConfiguration): Promise<google.cloud.tasks.v2.ITask | null> {
        if (!this.projectId) {
            return null
        }

        return await startTracer('CloudTask.Create', async (span) => {
            try {
                const { region, queue, scheduleTime } = taskOptions
                
                let seconds = 0
                if (scheduleTime) {
                    seconds = this.buildScheduleTime(taskOptions)
                }

                const task = new Task(taskOptions, seconds) as google.cloud.tasks.v2.ITask
                const tracerAgent = getTracerAgent()
                if (tracerAgent) {
                    tracerAgent.propagation.inject(tracerAgent.context.active(), task.httpRequest?.headers);
                }
                const parent = this.instance.queuePath(this.projectId!, region, queue)

                if (taskOptions.name) {
                    task.name = this.instance.taskPath(this.projectId!, region, queue, taskOptions.name)
                }
                
                const [response] = await this.instance.createTask(
                    { parent, task } as google.cloud.tasks.v2.ICreateTaskRequest, 
                    { timeout: 30000 }
                )

                span.setAttributes({
                    'cloudtask.queue': taskOptions.queue,
                    'cloudtask.region': taskOptions.region,
                    'cloudtask.success': true,
                    'cloudtask.url': taskOptions.url
                })

                return response
            } catch (error) {
                span.setAttributes({
                    'cloudtask.queue': taskOptions.queue,
                    'cloudtask.region': taskOptions.region,
                    'cloudtask.success': false,
                    'cloudtask.error': error instanceof Error ? error.message : 'Unknown error'
                })
                throw error
            }
        })
    }

    async deleteTask(name: string): Promise<void> {
        return await startTracer('cloudtask.delete', async (span) => {
            try {
                await this.instance.deleteTask({ name })
                span.setAttributes({
                    'cloudtask.name': name,
                    'cloudtask.success': true
                })
            } catch (error) {
                span.setAttributes({
                    'cloudtask.name': name,
                    'cloudtask.success': false,
                    'cloudtask.error': error instanceof Error ? error.message : 'Unknown error'
                })
                throw error
            }
        })
    }

    buildScheduleTime(taskOptions: TaskConfiguration): number {
        let seconds = 0
        if(taskOptions?.scheduleTime?.executeAt) {
            const milliseconds = Number(ms(taskOptions.scheduleTime.executeAt))
            seconds = milliseconds / 1000
        }
        if(taskOptions?.scheduleTime?.dateExecute) {
            const now = moment(new Date())
            const dateExecute = moment(taskOptions.scheduleTime.dateExecute)
            const duration = moment.duration(dateExecute.diff(now))
            seconds = duration.asSeconds()
        }
        if(taskOptions?.scheduleTime?.nanos) {
            seconds = Number(taskOptions.scheduleTime.nanos / 1000000000)
        }
        if(taskOptions?.scheduleTime?.seconds) {
            seconds = taskOptions.scheduleTime.seconds
        }

        const timestamp = taskOptions?.scheduleTime?.withTimeZone ? parseISO(newDate()).getTime() : Date.now()

        return seconds + timestamp / 1000
    }
}