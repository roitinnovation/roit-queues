
export type HttpMethod = 'POST' | 'PUT' | 'GET' | 'PATCH' | 'DELETE'
export class TaskConfiguration {
    name?: string
    url: string
    httpMethod: HttpMethod = 'POST'
    region: string | 'us-central1'
    queue: string
    dispatchDeadline?: {
        seconds?: number,
        nanos?: number
    }
    scheduleTime?: {
        seconds?: number
        nanos?: number
        dateExecute?: string
        executeAt?: string
        withTimeZone?: boolean
    }
    createTime?: {
        seconds?: number
    }
    headers?: unknown
    body?: unknown
    auth?: {
        oidcToken?: {
            serviceAccountEmail?: string
        }
    }
}